require 'spec_helper'

RSpec.describe 'bootstrap helpers', type: :helper do
  let(:errors) { { name: [], attachment: [] } }
  let(:object) { double("object", errors: errors, name: 'object-name') }
  let(:options) { { object: object } }

  RSpec.shared_examples "an input with type" do |type|
    it { is_expected.to have_tag("input.form-control[type=#{type}]") }

    context 'with errors' do
      let(:errors) { { name: ['is invalid'], attachment: ['is invalid'] } }

      it { is_expected.to have_tag("input.is-invalid[type=#{type}]") }
    end
  end

  RSpec.shared_examples "a select element" do
    it do
      is_expected.to have_tag('select.form-control') do
        with_tag('option[value=1]') do
          with_text 'One'
        end
      end
    end

    context 'with errors' do
      let(:errors) { { name: ['is invalid'] } }
      it { is_expected.to have_tag("select.is-invalid") }
    end
  end

  describe 'text_field' do
    subject do
      bootstrap_text_field(:post, :name, options)
    end

    it_behaves_like "an input with type", "text"
  end

  describe 'email_field' do
    subject do
      bootstrap_email_field(:post, :name, options)
    end

    it_behaves_like "an input with type", "email"
  end

  describe 'password_field' do
    subject do
      bootstrap_password_field(:post, :name, options)
    end

    it_behaves_like "an input with type", "password"
  end

  describe 'file_field' do
    subject do
      bootstrap_file_field(:post, :attachment, options)
    end

    it do
      is_expected.to have_tag('input[type="file"].form-control-file')
    end
  end

  describe 'select' do
    let(:options_for_select) { [["One", 1]] }

    subject do
      bootstrap_select(:post, :name, options_for_select, options, {})
    end

    it_behaves_like "a select element"
  end

  describe 'collection_select' do
    let(:collection) { [double(id: 1, name: 'One')] }
    subject do
      bootstrap_collection_select(:post, :name, collection, :id ,:name, options)
    end

    it_behaves_like "a select element"
  end

  describe 'text_area' do
    subject do
      bootstrap_text_area(:post, :name, options)
    end

    it do
      is_expected.to have_tag('textarea.form-control')
    end

    context 'with errors' do
      let(:errors) { { name: ['is invalid'] } }

      it do
        is_expected.to have_tag('textarea.is-invalid')
      end
    end
  end
end
