require 'spec_helper'

RSpec.describe '#bootstrap_control_group_wrap', type: :helper do
  let(:errors) { {} }
  let(:object) { double(errors: errors) }
  let(:options) { { object: object } }
  let(:content) do
    ::ActiveSupport::SafeBuffer.new('content')
  end

  subject do
    helper.bootstrap_control_group_wrap(:post, :name, content, options)
  end

  it do
    is_expected.to have_tag('div.form-group') do
      with_tag('label.control-label') do
        with_text 'Name'
      end
    end
  end

  context 'with option label: "Custom Label"' do
    let(:options) { { object: object, label: 'Custom Label' } }

    it do
      is_expected.to have_tag('div.form-group') do
        with_tag('label.control-label') do
          with_text 'Custom Label'
        end
      end
    end
  end

  context 'with errors' do
    let(:errors) { { name: ["has already been taken", "is reserved", "must be odd"] } }

    it do
      is_expected.to have_tag('div.form-group') do
        with_tag('div.invalid-feedback') do
          with_text " has already been taken, is reserved, and must be odd"
        end
      end
    end
  end

  context 'with option hint: "format matters"' do
    let(:options) { { object: object, hint: 'format matters' } }

    it do
      is_expected.to have_tag('div.form-group') do
        with_tag('small.text-muted') do
          with_text "format matters"
        end
      end
    end
  end

  context 'with errors and option hint: "format matters"' do
    let(:errors) { { name: ["has already been taken", "is reserved", "must be odd"] } }
    let(:options) { { object: object, hint: 'format matters' } }

    it do
      is_expected.to have_tag('div.form-group') do
        with_tag('div.invalid-feedback') do
          with_text " has already been taken, is reserved, and must be odd"
        end
      end
    end
  end
end
