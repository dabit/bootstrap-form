# Bootstrap Form

Form Helpers to make your form inputs [look like this](https://getbootstrap.com/docs/4.3/components/forms/).

Helps you to create beautiful mocks really quickly.

Works with Bootstrap 4 and Rails 5+.

For older versions of Bootstrap and Rails use older versions of this gem.

## Usage

Add the gem to your Gemfile

    gem 'bootstrap-form'

Bundle install

    bundle install

## Example

You write this:

    form_for @account do |f|
      f.bootstrap_text_field :name
    end


You get something like this:

    <div class="form-group">
      <label class="control-label" for="account_name">Name</label>
      <input class="form-control" id="account_name" name="account_name" size="30" type="text">
    </div>

Pretty straight forward.

## Custom Label

You can specify a custom label for the input by setting the label
option:

    form_for @account do |f|
      f.bootstrap_text_field :name, label: 'A custom label'
    end

Then, you get something like this:

    <div class="form-group">
      <label class="control-label" for="account_name">A custom label</label>
      <input class="form-control" id="account_name" name="account_name" size="30" type="text">
    </div>

## Current Helpers List

* bootstrap_text_field
* bootstrap_password_field
* bootstrap_collection_select
* bootstrap_select
* bootstrap_file_field
* bootstrap_text_area
* bootstrap_email_field

## Error handling

All fields will automatically add the classes to show errors with
bootstrap styling.

